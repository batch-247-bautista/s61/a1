import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

import { useContext, useEffect } from 'react';
import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  useEffect(() => {
    if (user.isAdmin !== null && user.isAdmin === true && window.location.pathname !== '/products') {
      window.location.href = '/products';
    }
  }, [user.isAdmin]);

  return (
    <Navbar bg="warning" expand="lg">
      <Navbar.Brand as={Link} to="/">
        Shoes Mio!
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to="/products">
            Products
          </Nav.Link>
          {user.isAdmin !== null && user.isAdmin === true && (
            <NavDropdown title="Admin" id="basic-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="/admin/add-product">
                Add Product
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/admin/inventory">
                Inventory
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/admin/user-settings">
                User Settings
              </NavDropdown.Item>
            </NavDropdown>
          )}
          {user.id !== null ? (
            <Nav.Link as={NavLink} to="/logout">
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/login">
                Login
              </Nav.Link>
              <Nav.Link as={NavLink} to="/register">
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

