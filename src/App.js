import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Admin from './pages/Admin';
import AddProduct from './pages/AddProduct';
import Inventory from './pages/Inventory';
import UserSettings from './pages/UserSettings';
import UpdateProductView from './pages/UpdateProductView';

import './App.css';

import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => { 
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products/>} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/login" element={<Login/>} />
            <Route path="/admin/*" element={<Admin/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/admin/add-product" element={<AddProduct/>} />
            <Route path="/admin/inventory" element={<Inventory/>} />
            <Route path="/admin/user-settings" element={<UserSettings/>} />
            <Route path="/update-product/:productId" element={<UpdateProductView/>} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}

export default App;
