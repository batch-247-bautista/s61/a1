import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2'; // import Swal

const UpdateProductView = () => {
  const { productId } = useParams();
  const [product, setProduct] = useState(null);
  const [message, setMessage] = useState('');

  useEffect(() => {
    const fetchProduct = async () => {
      const response = await fetch(`https://capstone-00002.onrender.com/products/${productId}/details`);
      const data = await response.json();
      setProduct(data);
    };
    fetchProduct();
  }, [productId]);

  const handleUpdate = (productId, name, description, price) => {
    const updateEndpoint = `https://capstone-00002.onrender.com/products/${productId}/update`;
    const updates = { name, description, price };
    fetch(updateEndpoint, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(updates),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setMessage('Product is successfully updated!');
        Swal.fire({
        icon: 'success',  
        title: 'Product updated!',
        confirmButtonText: 'OK'
      });

      })
      .catch((err) => console.error(err));
  };

  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>Update Product</h1>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          handleUpdate(
            product._id,
            event.target.name.value,
            event.target.description.value,
            event.target.price.value
          );
        }}
      >
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input type="text" id="name" className="form-control" defaultValue={product.name} required />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <textarea id="description" className="form-control" defaultValue={product.description} required />
        </div>
        <div className="form-group mb-3">
          <label htmlFor="price">Price</label>
          <input type="number" id="price" className="form-control" defaultValue={product.price} required />
        </div>
        <div className="d-flex justify-content-start align-items-center gap-2 mb-2">
          <button type="submit" className="btn btn-primary">
            Update Product
          </button>
          <Link to="/admin/inventory" className="btn btn-info">
            Back to Inventory
          </Link>
        </div>
      </form>
      {message && <div className="alert alert-success">{message}</div>}
    </div>
  );
};

export default UpdateProductView;
